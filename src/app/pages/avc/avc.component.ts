import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-avc',
  templateUrl: './avc.component.html',
  styleUrls: ['./avc.component.scss']
})
export class AvcComponent implements OnInit {

  fg: FormGroup;

  response: number;

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'age': new FormControl(null, [Validators.max(100), Validators.min(1), Validators.required]),
      'sexe': new FormControl(false),
    });
  }

  submit() {
    const values = this.fg.value;
    let params = new HttpParams();
    params = params.append('sexe', values.sexe);
    params = params.append('age', values.age);
    this.httpClient.get<number>('http://localhost:3000/calcul', {params}).subscribe(data => {
      this.response = data;
    });
  }

}
