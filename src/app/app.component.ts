import { Component } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  items: NbMenuItem[] = [
    { title: 'Accueil', icon: 'home', link: '/home' },
    { title: 'Projets', icon: 'calendar', children: [
      { title: 'AVC', link: '/avc' },

    ] }
  ];
}
